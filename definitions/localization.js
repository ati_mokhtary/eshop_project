var COOKIE = '__language';
var allowed = { fa: true, en: true };

LOCALIZE(function(req, res) {

	var language = req.query.language;

	// Set the language according to the querystring and store to the cookie
	if (language) {
		if (!allowed[language])
			return 'en';
		res.cookie(COOKIE, language, '2 days');
		return language;
	}

	language = req.cookie(COOKIE);
	if (language) {
		if (allowed[language])
			return language;
		return 'en';
	}

	// Sets the language according to user-agent
	language = req.language;

	if (language.indexOf('fa') > -1)
		return 'fa';

	return 'en';
});